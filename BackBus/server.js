const app = require("express")();
const cors = require("cors");
const bodyParser = require("body-parser");
const http = require('http').Server(app);
const mongoose = require("mongoose");

mongoose.set('useCreateIndex', true)

app.use(bodyParser.json());
app.use(cors());
app.use(
    bodyParser.urlencoded({
        extended: false
    })
)

// const mongoURI = 'mongodb+srv://admin:admin@cluster0-4ymgm.mongodb.net/data0'

const mongoURI = 'mongodb://127.0.0.1:27017/bus_localsiation';


mongoose.connect(mongoURI, {useNewUrlParser: true, useUnifiedTopology: true })
    .then(()=>console.log("MongoDB connected"))
    .catch(err => console.log(err));

var Users = require('./routes/user');
app.use('/api/bus', Users);

app.get('/test', (req, res)=>{
  res.json("Hello BELGHASSSEM")
})


const PORT = process.en || 4000;  
http.listen(PORT, ()=>{
    console.log(`Server is running on port ${PORT}`);
});


