const mongoose = require("mongoose")


const UserSchema = mongoose.Schema({
    first_name:{
        type: String,
        require: true,
    },
    last_name:{
        type: String,
        require: true,
    },
    phone_number:{
        type: Number,
        require: true,
    },
    username:{
        type: String,
        index: true,
        require: true,
        unique: true
    },
    password:{
        type: String,
        require : true
    },
    email:{
        type: String,
        require: true,
    },
});

module.exports = user = mongoose.model('user', UserSchema)



